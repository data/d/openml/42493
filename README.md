# OpenML dataset: airlines

https://www.openml.org/d/42493

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Albert Bifet, Elena Ikonomovska  
**Source**: [Data Expo competition](http://kt.ijs.si/elena_ikonomovska/data.html) - 2009  
**Please cite**:   

Airlines Dataset Inspired in the regression dataset from Elena Ikonomovska. The task is to predict whether a given flight will be delayed, given the information of the scheduled departure. For this version, the task was downsampled to 5 percent. The feature Flight was correctly encoded as a factor variable.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42493) of an [OpenML dataset](https://www.openml.org/d/42493). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42493/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42493/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42493/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

